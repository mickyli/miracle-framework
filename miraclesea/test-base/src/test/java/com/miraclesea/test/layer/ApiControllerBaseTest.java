package com.miraclesea.test.layer;

import java.io.IOException;
import java.io.StringWriter;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath*:config/spring/root/testContext.xml", 
	"classpath*:config/springmvc/webContext.xml"
})
@WebAppConfiguration
public abstract class ApiControllerBaseTest extends AbstractJUnit4SpringContextTests {
	
	@Autowired
	private WebApplicationContext webApplicationContext; 
	
	private MockMvc mockMvc;
	
	@Before
	public final void init() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		MockitoAnnotations.initMocks(this);
	}
	
	protected final MockMvc getMockMvc() {
		return mockMvc;
	}
	
	protected final String toJson(final Object obj) {
		StringWriter writer = new StringWriter();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		JsonGenerator jsonGenerator;
		try {
			jsonGenerator = objectMapper.getJsonFactory().createJsonGenerator(writer);
			jsonGenerator.writeObject(obj);
		} catch (final JsonProcessingException ex) {
			throw new RuntimeException(ex);
		} catch (final IOException ex) {
			throw new RuntimeException(ex);
		}
		return writer.toString();
	}
}
