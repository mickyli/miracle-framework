package com.miraclesea.component.log;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.miraclesea.framework.exception.LogicException;
import com.miraclesea.framework.exception.UserException;
import com.miraclesea.test.integrate.MockBaseTest;

public final class CallTraceTest extends MockBaseTest {
	
	private CallTrace callTrace;
	
	@Mock
	private ProceedingJoinPoint pjp;
	
	@Mock
	private Signature signature;
	
	@Before
	public void setUp() {
		callTrace = new CallTrace(pjp);
	}
	
	@Test
	public void getSignatureInfo() {
		when(signature.toLongString()).thenReturn("public void com.miraclesea.component.log.CallTrace.xxx");
		when(pjp.getSignature()).thenReturn(signature);
		assertThat(callTrace.getSignatureInfo(), is("Call: public void com.miraclesea.component.log.CallTrace.xxx"));
		verify(signature).toLongString();
		verify(pjp).getSignature();
	}
	
	@Test
	public void getArgumentsInfoWithoutArgument() {
		when(pjp.getArgs()).thenReturn(new Object[] {});
		assertThat(callTrace.getArgumentsInfo(), is("There is not any parameter"));
		verify(pjp).getArgs();
	}
	
	@Test
	public void getArgumentsInfoWithArguments() {
		when(pjp.getArgs()).thenReturn(new Object[] {1, 2.0, "C"});
		assertThat(callTrace.getArgumentsInfo(), is("There are 3 parameter(s), value(s) are: 1 -> 1, 2 -> 2.0, 3 -> C"));
		verify(pjp).getArgs();
	}
	
	@Test
	public void getReturnInfo() {
		assertThat(callTrace.getReturnInfo("Return Value"), is("Execute completed, the return value is: Return Value"));
	}
	
	@Test
	public void getUserExceptionInfo() {
		assertThat(callTrace.getUserExceptionInfo(new UserException("arg1", "arg2") {
			private static final long serialVersionUID = 5791849507258579682L;
		}), is("Got an user exception. Error code is: null, argument(s) are: arg1,arg2"));
	}
	
	@Test
	public void getLogicExceptionInfo() {
		assertThat(callTrace.getLogicExceptionInfo(new LogicException("Logic Eeception message") {
			private static final long serialVersionUID = 7077731141062217192L;
		}), is("Got an logic exception. It is: com.miraclesea.component.log.CallTraceTest$2: Logic Eeception message"));
	}
}
