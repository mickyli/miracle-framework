package com.miraclesea.component.log.asserts;

public final class MethodInstance {
	
	private final String methodName;
	private final Class<?> returnType;
	private Arguments arguments;
	private Object returnValue;
	private Exception ex;
	
	public MethodInstance(final String methodName, final Class<?> returnType, final Object returnValue, final Object... arguments) {
		this(methodName, returnType, arguments);
		this.returnValue = returnValue;
	}
	
	public MethodInstance(final String methodName, final Class<?> returnType, final Exception ex, final Object... arguments) {
		this(methodName, returnType, arguments);
		this.ex = ex;
	}
	
	private MethodInstance(final String methodName, final Class<?> returnType, final Object[] arguments) {
		this.methodName = methodName;
		this.returnType = returnType;
		this.arguments = new Arguments(arguments);
	}
	
	public String getMethodName() {
		return methodName;
	}
	
	public String getReturnType() {
		return returnType.getName();
	}
	
	public Arguments getArguments() {
		return arguments;
	}
	
	public Object getReturnValue() {
		return returnValue;
	}
	
	public boolean hasException() {
		return null != ex;
	}
	
	public Exception getException() {
		return ex;
	}
}
