package com.miraclesea.component;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.miraclesea.component.log.CallTraceTest;
import com.miraclesea.component.log.ErrorTraceTest;
import com.miraclesea.component.log.LoggerAspectTest;
import com.miraclesea.component.log.PerformanceTraceTest;

@RunWith(Suite.class)
@SuiteClasses({
	CallTraceTest.class, 
	PerformanceTraceTest.class, 
	ErrorTraceTest.class, 
	LoggerAspectTest.class
})
public final class AllLogTests { }
