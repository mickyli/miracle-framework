package com.miraclesea.module.rbac.dao;

import org.springframework.data.jpa.repository.Query;

import com.miraclesea.framework.dao.BaseJpaRepository;
import com.miraclesea.module.rbac.entity.Role;

public interface RoleDao extends BaseJpaRepository<Role, String> {
	
	Role findByName(String roleName);
	
	@Query("FROM Role r WHERE r.name=?1 AND r.id <> ?2")
	Role findByName(String roleName, String exceptedId);
}
