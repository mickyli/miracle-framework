package com.miraclesea.module.rbac.entity;

import com.miraclesea.framework.entity.BaseOptimisticLockAuditable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.DiscriminatorOptions;

@Getter
@Setter
@Entity
@Table(name = "role_permission")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "grant_type")
@DiscriminatorOptions(force = true)
public abstract class RolePermission extends BaseOptimisticLockAuditable<User> {
	
	private static final long serialVersionUID = -5470591687163776785L;
	
	@Column(name = "permission_name", length = 255)
	private String permissionName;
}
