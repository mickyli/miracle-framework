package com.miraclesea.module.rbac.vo;

public enum RbacPermission implements Permission {
	
	AssignRolePermission,
	DenyRolePermission,
	ExtendRole,
	DismissExtendRole;
	
	@Override
	public String toPermissionString() {
		return PermissionUtil.toPermissionString(this);
	}
}
